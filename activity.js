//a. What directive is used by Node.js in loading the modules it needs?
	/*
		Answer: "require" directive

	*/


//b. What Node.js module contains a method for server creation?

	/*
		Answer: http module
	
	*/


//c. What is the method of the http object responsible for creating a server using
	/*
		Answer: createServer() method
		
	*/


//d. Node.js?
	/*
		Answer: An open-source, javascript runtime environment for creating server-side applications.
	*/


//e. What method of the response object allows us to set status codes and content types?
	/*
		Answer: writeHead() method
		
	*/


//f. Where will console.log() output its contents when run in Node.js?
	/*
		Answer: git bash
	*/


//g. What property of the request object contains the address endpoint?
	/*
		Answer: port
	*/