

const http = require("http")
let url = require("url")

const port = 3000

const server = http.createServer((request,response)=>{

	if(request.url == '/login'){
		response.writeHead(200,{'Content-Type' : 'text/plain'})
		response.end('Welcome to Login Page')

	}else{
		response.writeHead(200,{'Content-Type' : 'text/plain'})
		response.end(`I'm sorry the page that you are looking for cannot be found.`)
	}

})

server.listen(port)
console.log(`Server now accessible at localhost: ${port}`)